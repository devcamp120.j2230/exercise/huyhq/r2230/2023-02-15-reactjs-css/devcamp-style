const container = {
    marginTop: "100px"
};
const dcContainer = {
    margin: "0 auto",
    width: "800px",
    textAlign: "center",
    border: "1px solid #cacaca",
    backgroundColor: "bisque"
}

const dcImage = {
    borderRadius: "50%",
    marginTop: "-64px",
    width: "128px"
}

const dcQote = {
    fontSize: "18px"
}

const dcName = {
    color: "brown"
}


export { container, dcContainer, dcImage, dcQote, dcName };