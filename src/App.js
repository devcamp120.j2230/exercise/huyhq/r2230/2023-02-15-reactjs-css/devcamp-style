import avatar from './asset/image/48.jpg';
import './App.css';

import {container, dcContainer, dcImage, dcQote, dcName} from './inlineStyle'

// import cssModule from './App.module.css';

function App() {
  return (
    <div className='container'>
      <div className='dc-container'>
        <img src={avatar} className='dc-image' alt='Tammy'/>
        <p className='dc-quote'>This is one of the best developer blogs on the planet! I read it daily to improve my skills</p>
        <p className='dc-name'><b>Tammy Stevens</b> Front End Developer</p>
      </div>
    </div>

  );
}

export default App;
